package com.legacy.dash;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.Enchantment.Rarity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;

@EventBusSubscriber(modid = DashMod.MODID, bus = Bus.MOD)
public class DashRegistry
{
	public static final Enchantment DASHING = new DashingEnchantment(Rarity.RARE, EquipmentSlotType.FEET);

	@SubscribeEvent
	public static void registerEnchantments(Register<Enchantment> event)
	{
		register(event.getRegistry(), "dashing", DASHING);
	}

	private static <T extends IForgeRegistryEntry<T>> void register(IForgeRegistry<T> registry, String name, T object)
	{ 
		object.setRegistryName(DashMod.locate(name));
		registry.register(object);
	}
}
