package com.legacy.dash;

import net.minecraft.client.Minecraft;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraftforge.client.event.InputEvent;
import net.minecraftforge.event.TickEvent.ClientTickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class DashEntityEvents
{
	byte cooldown = 0;

	@SubscribeEvent
	public void onKeyPressed(InputEvent.KeyInputEvent event)
	{
		Minecraft mc = Minecraft.getInstance();

		if (mc.player == null)
			return;

		PlayerEntity player = mc.player;

		float enchantmentLevel = EnchantmentHelper.getMaxEnchantmentLevel(DashRegistry.DASHING, player);

		if (DashMod.DashClient.DASH_KEYBIND.isKeyDown() && cooldown <= 0 && enchantmentLevel > 0 && player.moveForward != 0 && !player.isInWaterOrBubbleColumn())
		{
			for (int i = 0; i < 20; ++i)
			{
				double d0 = mc.world.rand.nextGaussian() * 0.02D;
				double d1 = mc.world.rand.nextGaussian() * 0.02D;
				double d2 = mc.world.rand.nextGaussian() * 0.02D;
				mc.world.addParticle(ParticleTypes.POOF, player.getPosX() + (double) (player.world.rand.nextFloat() * player.getWidth() * 2.0F) - (double) player.getWidth() - d0 * 10.0D, player.getPosY() + (double) (player.world.rand.nextFloat() * player.getHeight()) - d1 * 10.0D, player.getPosZ() + (double) (player.world.rand.nextFloat() * player.getWidth() * 2.0F) - (double) player.getWidth() - d2 * 10.0D, d0, d1, d2);
			}

			Vector3d playerLook = player.getLook(1);
			Vector3d dashVec = new Vector3d(playerLook.getX(), player.getMotion().getY(), playerLook.getZ());
			player.setMotion(dashVec);

			player.playSound(SoundEvents.ENTITY_PHANTOM_FLAP, 1.0F, 2.0F);
			cooldown = 50;
		}
	}

	@SubscribeEvent
	public void onClientTick(ClientTickEvent event)
	{
		if (cooldown > 0)
			--cooldown;
	}
}
